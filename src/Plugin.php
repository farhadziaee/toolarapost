<?php

return [
    'name' => 'Posts',
    'version' => '1.0.0',
    'author' => 'Farhad Ziaee',
    'description' => 'This is for develop posts in CMS Toolara',
    'package' => 'toolara/post',
    'namespace' => 'Toolara\Post',
    'author_email'=>'fardzia@gmail.com',
    'type' => 'plugin',
    'url' => 'https://gitlab.com',
    'providers' => [
        '\Toolara\Post\ToolaraPostServiceProvider'
    ]
];
