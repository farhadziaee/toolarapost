<?php

use Illuminate\Support\Facades\Route;



Route::group(['namespace' => 'Toolara\Post\Http\Controllers','middleware'=>'toolara'], function () {
    Route::prefix("/backend/post")->middleware('auth')->group(function () {
        Route::get('/', 'PostController@post')->name('post');
        Route::get('/categories', 'PostController@categories')->name('post_categories');
        Route::get('/categories/new', 'PostController@newCategory')->name('new_category');
        Route::post('/categories/new', 'PostController@saveCategory')->name('add_category');
        Route::get('/categories/edit/{id}', 'PostController@editCategory')->name('edit_category');
        Route::get('/categories/delete/{id}', 'PostController@deleteCategory')->name('delete_category');

        Route::get('/new', 'PostController@newPost')->name('new_post');
    });
});
