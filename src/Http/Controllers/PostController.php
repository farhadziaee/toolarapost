<?php

namespace Toolara\Post\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Toolara\Core\Models\Term;

class PostController extends Controller
{
    public function __construct()
    {
    }

    public function post()
    {
        return view("toolara_post::post");
    }

    public function categories()
    {
        return view("toolara_post::categories");
    }

    public function newCategory()
    {
        return view("toolara_post::new_category");
    }

    public function saveCategory(Request $post): \Illuminate\Http\RedirectResponse
    {

        $term = new Term;

        $post->validate($this->newCategoryValidateArray());

        if ($post->edit_id) {
            $term->where('id', $post->edit_id)
                ->update([
                    'name' => $post->name,
                    'slug' => Str::of($post->slug ?? $post->name)->slug()
                ]);
        } else {
            $term->name = $post->name;
            $term->slug = Str::of($post->slug ?? $post->name)->slug();
            $term->save();
        }
        return redirect()->route('post_categories');
    }

    private function newCategoryValidateArray(): array
    {
        return [
            'name' => 'required|max:255|unique:tl_terms',
        ];
    }

    public function editCategory($id)
    {
        return view("toolara_post::new_category", ['id' => $id]);
    }

    public function deleteCategory($id): \Illuminate\Http\RedirectResponse
    {
        $term = new Term;
        $term->where('id', '=', $id)->delete();
        return redirect()->route('post_categories');
    }

    public function newPost(){
        return view("toolara_post::new_post");
    }

}
