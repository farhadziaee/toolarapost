
@extends('core::master')
@section('title',__('toolara_post_messages.titles.post'))

@section('content')
    <div class="content-wrapper">
        <div class="content">
                @include('toolara_post::new_category_content')
        </div>
    </div>
@endsection
