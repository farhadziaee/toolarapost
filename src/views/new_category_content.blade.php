
    @php
        $id = $id ?? false;
        $term = $id ? \Toolara\Core\Models\Term::where('id','=',$id)->first() : false;
    @endphp
    <div class="">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">
                            @if($term)
                                Edit Categories
                            @else
                                New Categories
                            @endif
                        </h1>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="">
                    <form action="{{ route('new_category') }}" method="post">
                        @csrf
                        @if($term)
                            <input type="hidden" name="edit_id" value="{{$term->id}}">
                        @endif
                        <div class="form-group">
                            <label for="category_name">Category Name</label>
                            <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="category_name"
                                   placeholder="Category Name" value="{{ $term ? $term->name : '' }}">
                            <small id="category_name_help" class="invalid-feedback">@if($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif</small>
                        </div>
                        <div class="form-group">
                            <label for="category_slug">Slug</label>
                            <input name="slug" type="text" class="form-control " id="category_slug"
                                   placeholder="Slug" value="{{ $term ? $term->slug : '' }}">
                            <small id="category_slug_help" class="invalid-feedback">@if($errors->has('slug'))
                                    <div class="error">{{ $errors->first('slug') }}</div>
                                @endif</small>
                        </div>

                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>

