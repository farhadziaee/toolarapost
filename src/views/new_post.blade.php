@extends('core::master')
@title(__('toolara_post_messages.titles.new_post'))

@section('script')
    <script src="{{ asset('assets/toolarastyle/adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        })
    </script>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="col-12">
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0">New Post</h1>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>

                </div>
                <div class="content">
                    <div class="mb-3">
                        <label for="post_title" class="form-label">Title</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>
                    @push('include_header')
                        <link rel="stylesheet"
                              href="{{ asset('assets/toolarastyle/adminlte/plugins/summernote/summernote-bs4.min.css') }}">
                    @endpush
                    <div class="mb-3">
                        <label for="post_title" class="form-label">Content</label>
                        <textarea class="form-control" id="summernote"><br/><br/><br/><br/></textarea>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>

@endsection
