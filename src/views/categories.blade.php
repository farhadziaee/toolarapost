@extends('core::master')
@title(__('toolara_post_messages.titles.categories'))
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
            <div class="row">
                <div class="col-lg-12 col-xl-6">
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h1 class="m-0">Categories</h1>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </div>
                    <table class="table bg-white">
                        <thead>
                        <tr>
                            <th style="width: 50px">No.</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Posts</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\Toolara\Core\Models\Term::all() as $index=>$term)
                            <tr>
                                <th>{{$index+1}}</th>
                                <td class="text-bold">{{$term->name}}</td>
                                <td class="text-sm font-italic">{{$term->slug}}</td>
                                <td>{{$term->id}}</td>
                                <td>
                                    <a title="Edit" href="{{ route('edit_category',['id'=>$term->id]) }}"
                                       class="btn btn-xs btn-primary">
                                        <span class="fas fa-edit"></span>
                                    </a>
                                    <a onclick="return confirm('Do you want delete this category?')" title="Delete"
                                       href="{{ route('delete_category',['id'=>$term->id]) }}"
                                       class="btn btn-xs btn-danger">
                                        <span class="fas fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 col-xl-6 border-left">
                    @include('toolara_post::new_category_content')
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>

@endsection
